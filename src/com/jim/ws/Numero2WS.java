package com.jim.ws;

import javax.jws.*;

import com.conectar.insertar;

@WebService
public class Numero2WS {
	insertar ins = new insertar();

	@WebResult(name = "Resultado")
	@WebMethod(operationName = "Agregar")
	public String sumar(@WebParam(name = "Numero1") Integer numero1, @WebParam(name = "Numero2") Integer numero2) {
		int resultado = numero1 + numero2;
		String data = ins.add(numero1, numero2, resultado);
		return data;
	} 

	public String restar(@WebParam(name = "Numero1") int numero1, @WebParam(name = "Numero2") int numero2) {
		int resultado = numero1 - numero2;
		String data = ins.restar(numero1, numero2, resultado);
		return data;
	}

	public String multiplicar(@WebParam(name = "Numero1") int numero1, @WebParam(name = "Numero2") int numero2) {
		int resultado = numero1 * numero2;
		String data = ins.multiplicar(numero1, numero2, resultado);
		return data;
	}

	public String dividir(@WebParam(name = "Numero1") int numero1, @WebParam(name = "Numero2") int numero2) {
		int resultado = numero1 / numero2;
		String data = ins.dividir(numero1, numero2, resultado);
		return data;

	}
}
