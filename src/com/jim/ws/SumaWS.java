package com.jim.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ws.rs.GET;
//import javax.ws.rs.POST;
///import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
import com.conectar.insertar;

//@Path ("suma")
public class SumaWS {
	insertar ins = new insertar();
//@POST
//@Path("/{Numero1}/{Numero2}")
//@Produces("application/json")
@WebResult(name = "Resultado")
@WebMethod(operationName = "Agregar")
public String sumar(@WebParam(name = "Numero1") Integer numero1, @WebParam(name = "Numero2") Integer numero2) {
	int resultado = numero1 + numero2;
	String data = ins.add(numero1, numero2, resultado);
	return data;
} 

public String restar(@WebParam(name = "Numero1") int numero1, @WebParam(name = "Numero2") int numero2) {
	int resultado = numero1 - numero2;
	String data = ins.restar(numero1, numero2, resultado);
	return data;
}

public String multiplicar(@WebParam(name = "Numero1") int numero1, @WebParam(name = "Numero2") int numero2) {
	int resultado = numero1 * numero2;
	String data = ins.multiplicar(numero1, numero2, resultado);
	return data;
}

public String dividir(@WebParam(name = "Numero1") int numero1, @WebParam(name = "Numero2") int numero2) {
	int resultado = numero1 / numero2;
	String data = ins.dividir(numero1, numero2, resultado);
	return data;

}
}