package com.conectar;

public interface Insersion {
	public String add(Integer numero1,Integer numero2,Integer resultado);
	public String restar(Integer numero1,Integer numero2,Integer resultado);
	public String multiplicar(Integer numero1,Integer numero2,Integer resultado);
	public String dividir(Integer numero1,Integer numero2,Integer resultado);
}
